<?php $data['is_crud_table'] = $is_crud_table; ?>
<?php $this->load->view('admin/header'); ?>
        
        <style type='text/css'>
        body
        {
            font-family: Arial;
            font-size: 14px;
        }
        a {
            color: blue;
            text-decoration: none;
            font-size: 14px;
        }
        a:hover
        {
            text-decoration: underline;
        }

        input {
            height: 30px !important;
        }
        </style>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Establishments <small>Manage List</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li> Home </li>
                            <li class="active"> <a href="<?php echo site_url(); ?>administrator/establishments">Establishments </a></li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $output->output; ?>
                    <div class="col-lg-12">
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>


<?php $this->load->view('admin/footer', $data); ?>