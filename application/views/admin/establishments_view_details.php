<?php $this->load->view('admin/header'); ?>

<style type="text/css">
    td {
        font-size: 12px;
    }
</style>


        <div id="page-wrapper">

            <div class="container-fluid">
                <?php foreach($details as $row): ?>

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo ucfirst($row['name']) ?> <small><?php echo ucfirst($small_title); ?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <a href="<?php echo base_url(); ?>">Home</a> / 
                                <a href="<?php echo base_url(); ?>admin/establishments">Establishments</a> / 
                                <?php echo ucfirst($row['name']) ?>
                            </li>
                        </ol>
                    </div>
                </div>

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Establishment Information
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <td colspan="2">
                                            <img src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['cover_image'] ?>" style="height: auto; width: 100%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td><?php echo ucfirst($row['name']) ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Owner</strong></td>
                                        <td><?php echo ucfirst($row['owner']) ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address</strong></td>
                                        <td><?php echo $row['street'].(empty($row['city_name'])?" ":", ").$row['city_name'].(empty($row['province_name'])?" ":", ").$row['province_name'].(empty($row['region_name'])?" ":", ").$row['region_name'].(empty($row['code'])?" ":", Philippines ").$row['code']; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap;"><strong>Contact Details</strong></td>
                                        <td>
                                            <?php echo $row['telephone_number'].(empty($row['telephone_number'])?" ":" <br> ").$row['cellphone_number'].(empty($row['cellphone_number'])?" ":" <br> ").$row['email']; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Excerpt</strong></td>
                                        <td><?php echo ucfirst($row['short_summary']) ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Description</strong></td>
                                        <td><?php echo ucfirst($row['description']) ?></td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap;"><strong>Booking Details</strong></td>
                                        <td><?php echo ucfirst($row['booking_details']) ?></td>
                                    </tr>
                                </table>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Other Details
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <td colspan="2">
                                            <img src="<?php echo base_url(); ?>assets/images/img/<?php echo $row['image'] ?>" style="height: auto; width: 100%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Wifi Ready</strong></td>
                                        <td><?php echo ucfirst($row['wifi_ready']) ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Cards Accepted</strong></td>
                                        <td><?php echo ucfirst($row['cards_accepted']) ?></td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap;"><strong>Price Range</strong></td>
                                        <td><?php echo ucfirst($row['price']) ?></td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap;"><strong>Working Hours</strong></td>
                                        <td><?php echo $row['working_hours'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap;"><strong>Site Page</strong></td>
                                        <td><a href="<?php echo base_url() ?>f/<?php echo $row['friendly_url'] ?>">http://where.net.ph/f/<?php echo $row['friendly_url'] ?></a></td>
                                    </tr>
                                </table>

                                <center>
                                <a href="#" class="btn btn-info"><i class="fa fa-star"></i> Feature this page</a>
                                <a href="#" class="btn btn-success"><i class="fa fa-pencil"></i> Edit</a>
                                </center>
                                
                            </div>
                        </div>
                    </div>
                </div>

                
                <?php endforeach; ?>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>

<?php $this->load->view('admin/footer'); ?>

<!-- INSERT JQUERY HERE -->
<script type="text/javascript">
    $(document).ready( function () {
        $('#esttable').DataTable({
            "bJQueryUI": true
        });
    } );
</script>