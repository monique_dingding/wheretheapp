<?php $this->load->view('admin/header'); ?>

<style type="text/css">
    td {
        font-size: 12px;
    }
</style>


        <div id="addUserModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" id="userInfoForm" action="<?php echo base_url(); ?>admin/insert_user" role="form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-user"></i> User's Information</h4>
                        </div>
                        <div class="modal-body">
                            
                                <div class="form-group">
                                    <label for="first-name">First Name:</label>
                                    <input type="text" class="form-control" id="first-name" name="fname">
                                </div>
                                <div class="form-group">
                                    <label for="last-name">Last Name:</label>
                                    <input type="text" class="form-control" id="last-name" name="lname">
                                </div>
                                <div class="form-group">
                                    <label for="username">Username:</label>
                                    <input type="text" class="form-control" id="username" name="username">
                                </div>
                                <div class="form-group">
                                    <label for="email">E-mail Address:</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="cel-num">Cellphone Number:</label>
                                    <input type="text" class="form-control" id="cel-num" name="cel-num">
                                </div>
                                <div class="form-group">
                                    <label for="tel-num">Telephone Number:</label>
                                    <input type="text" class="form-control" id="tel-num" name="tel-num">
                                </div>
                                <div class="form-group">
                                    <label for="birth">Birthdate:</label>
                                    <input type="date" class="form-control" id="birth" name="birthdate">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control" id="pwd" name="pwd">
                                </div>
                                <div class="form-group">
                                    <label for="repwd">Retype Password:</label>
                                    <input type="password" class="form-control" id="repwd" >
                                </div>
                                <div class="form-group" id="divCheckPasswordMatch"></div>
                                <div>
                                    <label>Role:</label>
                                    <select name="role_id" class="form-control">
                                        <option value="1" selected>Client</option>
                                        <option value="2" >Admin</option>
                                        <option value="3" >Super Admin</option>
                                    </select>
                                </div>
                                <div style="margin-top: 20px;">
                                    <label> Status:</label>
                                    <label class="radio-inline"><input type="radio" value="1" name="status">Approved</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="status">Pending</label>
                                </div>
                            
                            <p class="text-warning"><small>If you don't save, your changes will be lost.</small></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <i class="fa fa-users"></i> <?php echo ucfirst($title); ?> <small><?php echo ucfirst($small_title); ?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                Home / Dashboard
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <a href="#addUserModal" role="modal" data-toggle="modal"><i class="fa fa-plus-circle"></i> Add User</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <table id="esttable" class="table table-condensed table-bordered table-striped">
                            <thead>
                                <td><strong>First Name</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Last Name</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Username</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Email</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Phone Number </strong>  <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Birthdate</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Status</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Action</strong></td>
                            </thead>
                            <tbody>
                                <?php foreach($users as $row): ?>
                                <tr>
                                    <td><?php echo $row['fname'] ?></td>
                                    <td><?php echo $row['lname'] ?></td>
                                    <td><?php echo $row['username'] ?></td>
                                    <td><?php echo $row['email'] ?></td>
                                    <td><?php echo (isset($row['tel-num'])?$row['tel-num']:"").(isset($row['cel-num'])?" / ".$row['cel-num']:""); ?> </td>
                                    <td><?php echo $row['birthdate'] ?></td>
                                    <td><?php echo ($row['status']==1?"Approved":"Awaiting for Approval"); ?></td>
                                    <td><a href="<?php echo base_url();?>admin/user/<?php echo $row['user_id'];?>"><i class="fa fa-pencil-square-o"></i></a> &nbsp; <a href="<?php echo base_url();?>admin/delete_user/<?php echo $row['user_id'];?>" onClick="return confirm('Are you sure you want to delete the user <?php echo $row['fname'].' '.$row['lname']; ?>? This option is not reversible.')"><i class="fa fa-trash"></i></a></td>
                              

                              </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>

<?php $this->load->view('admin/footer'); ?>

<!-- INSERT JQUERY HERE -->
<script type="text/javascript">
    $(document).ready( function(){

    $(document).ready( function () {
        $('#esttable').DataTable({
            "bJQueryUI": true
        });

        var form = $("#userInfoForm");
        $("#userInfoForm").submit(function(event){
            event.preventDefault();
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:$("#userInfoForm").serialize(),//only input
                success: function(response){
                    alert(response);
                    if(response=='Data Inserted Successfully')
                        $('#addUserModal').modal('hide');  
                }
            });
        });

        $('#addUserModal').on('hidden.bs.modal', function (e) {
            location.reload();
        });

        $("#repwd").keyup(checkPasswordMatch);

        function checkPasswordMatch() {
            var password = $("#pwd").val();
            var confirmPassword = $("#repwd").val();

            if (password != confirmPassword)
                $("#divCheckPasswordMatch").html("<p class='text-danger'>Passwords do not match!</p>");
            else
                $("#divCheckPasswordMatch").html("<p class='text-success'>Passwords match.</p>");
        }

    });

</script>