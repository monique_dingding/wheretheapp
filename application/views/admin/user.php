<?php $this->load->view('admin/header'); ?>

<style type="text/css">
    td {
        font-size: 12px;
    }
</style>
<!--
        <div id="addUserModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" id="userInfoForm" action="<?php echo base_url(); ?>admin/insert_user" role="form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-user"></i> User's Information</h4>
                        </div>
                        <div class="modal-body">
                            
                                <div class="form-group">
                                    <label for="first-name">First Name:</label>
                                    <input type="text" class="form-control" id="first-name" name="fname">
                                </div>
                                <div class="form-group">
                                    <label for="last-name">Last Name:</label>
                                    <input type="text" class="form-control" id="last-name" name="lname">
                                </div>
                                <div class="form-group">
                                    <label for="username">Username:</label>
                                    <input type="text" class="form-control" id="username" name="username">
                                </div>
                                <div class="form-group">
                                    <label for="email">E-mail Address:</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="cel-num">Cellphone Number:</label>
                                    <input type="text" class="form-control" id="cel-num" name="cel-num">
                                </div>
                                <div class="form-group">
                                    <label for="tel-num">Telephone Number:</label>
                                    <input type="text" class="form-control" id="tel-num" name="tel-num">
                                </div>
                                <div class="form-group">
                                    <label for="birth">Birthdate:</label>
                                    <input type="date" class="form-control" id="birth" name="birthdate">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control" id="pwd" name="pwd">
                                </div>
                                <div class="form-group">
                                    <label for="repwd">Retype Password:</label>
                                    <input type="password" class="form-control" id="repwd" >
                                </div>
                                <div class="form-group" id="divCheckPasswordMatch"></div>
                                <div>
                                    <label> Status:</label>
                                    <label class="radio-inline"><input type="radio" value="1" name="status">Approved</label>
                                    <label class="radio-inline"><input type="radio" value="0" name="status">Pending</label>
                                </div>
                            
                            <p class="text-warning"><small>If you don't save, your changes will be lost.</small></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    -->
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <i class="fa fa-users"></i> <?php echo ucfirst($title); ?> <small><?php echo ucfirst($small_title); ?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                Home / Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
                <!--
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#addUserModal" role="modal" data-toggle="modal"><i class="fa fa-plus-circle"></i> Add User</a>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <form method="post" id="updateUserInfoForm" action="<?php echo base_url(); ?>admin/insert_user/<?php echo $user[0]['user_id']; ?>" role="form">    
                            <h4><i class="fa fa-user"></i> User's Information</h4>
                            <div class="form-group">
                                <label for="first-name">First Name:</label>
                                <input type="text" class="form-control" id="first-name" name="fname" value="<?php echo set_value('fname', isset($user[0]['fname']) ? $user[0]['fname'] : '');?>">
                            </div>
                            <div class="form-group">
                                <label for="last-name">Last Name:</label>
                                <input type="text" class="form-control" id="last-name" name="lname" value="<?php echo set_value('lname', isset($user[0]['lname']) ? $user[0]['lname'] : '');?>">
                            </div>
                            <div class="form-group">
                                <label for="username">Username:</label>
                                <input type="text" class="form-control" id="username" name="username"
                                value="<?php echo set_value('username', isset($user[0]['username']) ? $user[0]['username'] : '');?>">
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail Address:</label>
                                <input type="email" class="form-control" id="email" name="email" value="<?php echo set_value('email', isset($user[0]['email']) ? $user[0]['email'] : '');?>">
                            </div>
                            <div class="form-group">
                                <label for="cel-num">Cellphone Number:</label>
                                <input type="text" class="form-control" id="cel-num" name="cel-num" value="<?php echo set_value('cel-num', isset($user[0]['cel-num']) ? $user[0]['cel-num'] : '');?>">
                            </div>
                            <div class="form-group">
                                <label for="tel-num">Telephone Number:</label>
                                <input type="text" class="form-control" id="tel-num" name="tel-num" value="<?php echo set_value('tel-num', isset($user[0]['tel-num']) ? $user[0]['tel-num'] : '');?>">
                            </div>
                            <div class="form-group">
                                <label for="birth">Birthdate:</label>
                                <input type="date" class="form-control" id="birth" name="birthdate" value="<?php echo set_value('birthdate', isset($user[0]['birthdate']) ? $user[0]['birthdate'] : '');?>">
                            </div>
                            <!--
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" class="form-control" id="pwd" name="pwd" value="<?php echo set_value('pwd', isset($user[0]['pwd']) ? $user[0]['pwd'] : '');?>">
                            </div>
                            <div class="form-group">
                                <label for="repwd">Retype Password:</label>
                                <input type="password" class="form-control" id="repwd" >
                            </div>
                            <div class="form-group" id="divCheckPasswordMatch"></div>-->
                            <div>
                                <label>Role:</label>
                                <select name="role_id" class="form-control">
                                    <option value="1" <?php echo (($user[0]['role_id']==1) ? 'selected' : '');?>>Client</option>
                                    <option value="2" <?php echo (($user[0]['role_id']==2) ? 'selected' : '');?>>Admin</option>
                                    <option value="3" <?php echo (($user[0]['role_id']==3) ? 'selected' : '');?> >Super Admin</option>
                                </select>
                            </div>
                            <div style="margin-top: 20px;">
                                <label>Status:</label>
                                <label class="radio-inline"><input type="radio" value="1" name="status" <?php echo (($user[0]['status']==1) ? 'checked' : '');?>>Approved</label>
                                <label class="radio-inline"><input type="radio" value="0" name="status" <?php echo (($user[0]['status']==0) ? 'checked' : '');?>>Pending</label>
                            </div>    
                            <p class="text-warning"><small>If you don't save, your changes will be lost.</small></p>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>

<?php $this->load->view('admin/footer'); ?>

<!-- INSERT JQUERY HERE -->
<script type="text/javascript">
    $(document).ready( function () {
        $('#esttable').DataTable({
            "bJQueryUI": true
        });

        var form = $("#userInfoForm");
        $("#userInfoForm").submit(function(event){
            event.preventDefault();
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:$("#userInfoForm").serialize(),//only input
                success: function(response){
                    alert(response);
                    if(response=='Data Inserted Successfully')
                        $('#addUserModal').modal('hide');  
                }
            });
        });

        var form = $("#updateUserInfoForm");
        $("#updateUserInfoForm").submit(function(event){
            event.preventDefault();
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:$("#updateUserInfoForm").serialize(),//only input
                success: function(response){
                    alert(response);
                }
            });
        });

        $('#addUserModal').on('hidden.bs.modal', function (e) {
            location.reload();
        });

        $("#repwd").keyup(checkPasswordMatch);

        function checkPasswordMatch() {
            var password = $("#pwd").val();
            var confirmPassword = $("#repwd").val();

            if (password != confirmPassword)
                $("#divCheckPasswordMatch").html("<p class='text-danger'>Passwords do not match!</p>");
            else
                $("#divCheckPasswordMatch").html("<p class='text-success'>Passwords match.</p>");
        }

    });

</script>