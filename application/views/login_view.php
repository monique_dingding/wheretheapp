<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
   <title>Simple Login with CodeIgniter</title>
 </head>
 <body>
   <h1>Simple Login with CodeIgniter</h1>
   <?php echo validation_errors(); ?>
   <?php echo form_open('verifylogin'); ?>
     <label for="username">Username:</label>
     <input type="text" size="20" id="username" name="username"/>
     <br/>
     <label for="password">Password:</label>
     <input type="password" size="20" id="passowrd" name="password"/>
     <br/>
     <input type="submit" value="Login"/>
   </form>
 </body>
</html>


-->

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Where - Administrator</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/login/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/login/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/login/css/form-elements.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/login/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/login/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/login/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/login/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/login/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/login/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
          
            <div class="inner-bg">
                <div class="container">
                    
                    <div class="row">
                      <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                          
                          <div class="form-box">
                            <div class="form-top">
                              <div class="form-top-left">
                                <h3>Login</h3>
                                  <p>Enter username and password to log on:</p>
                              </div>
                              <div class="form-top-right">
                                <i class="fa fa-lock"></i>
                              </div>
                              </div>
                              <div class="form-bottom">
                                <?php
                                $attributes = array('class' => 'login-form', 'role' => 'form');
                                echo validation_errors();
                                echo form_open('verifylogin',$attributes); ?>
                              <div class="form-group">
                                <label class="sr-only" for="form-username">Username</label>
                                  <input type="text" name="username" placeholder="Username..." class="form-username form-control" id="form-username">
                                </div>
                                <div class="form-group">
                                  <label class="sr-only" for="form-password">Password</label>
                                  <input type="password" name="pwd" placeholder="Password..." class="form-password form-control" id="form-password">
                                </div>
                                <button type="submit" class="btn">Sign in!</button>
                            </form>
                          </div>
                        </div>
                    
                      <!--<div class="social-login">
                            <h3>...or login with:</h3>
                            <div class="social-login-buttons">
                              <a class="btn btn-link-2" href="#">
                                <i class="fa fa-facebook"></i> Facebook
                              </a>
                              <a class="btn btn-link-2" href="#">
                                <i class="fa fa-twitter"></i> Twitter
                              </a>
                              <a class="btn btn-link-2" href="#">
                                <i class="fa fa-google-plus"></i> Google Plus
                              </a>
                            </div>
                          </div>-->
                          
                        </div>
                        <div class="col-sm-3"></div>
                        <!--<div class="col-sm-1 middle-border"></div>
                        <div class="col-sm-1"></div>
                          
                        <div class="col-sm-5">
                          
                        </div>-->
                    </div>
                    
                </div>
            </div>
            
        </div>

        <!-- Footer -->
        <footer>
          <div class="container">
            <div class="row">
              
              
              
            </div>
          </div>
        </footer>

        <!-- Javascript -->
        <script src="<?php echo base_url(); ?>assets/login/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/login/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/login/js/jquery.backstretch.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/login/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>