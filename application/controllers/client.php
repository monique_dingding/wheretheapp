<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Client extends CI_Controller {

  public $username = "";
  public $data = array();
  function __construct() {
    parent::__construct();
    $this->username = "";
    $this->load->database();

      if($this->session->userdata('logged_in'))
       {
          $session_data = $this->session->userdata('logged_in');
          $data['username'] = $session_data['username'];
          $this->username = $session_data['username'];
          if($session_data['role_id']!=1)
            redirect('login', 'refresh');
       }
       else
       {
         //If no session, redirect to login page
         redirect('login', 'refresh');
       }
    }

 function index()
 {
    $data['username'] = $this->username;
    $this->load->view('client/home', $data);   
 }

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('admin', 'refresh');
 }

}

?>