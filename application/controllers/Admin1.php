<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
        parent::__construct();

        $this->load->database();
		$this->load->model('admin_model');
    }


	public function index()
	{
		$this->load->view('admin/dashboard');
	}

	public function establishments() {

		$data['title'] = "Establishments";
		$data['small_title'] = "Manage list";
		$data['establishments'] = $this->admin_model->get_establishments();

		// echo "<pre>";
		// print_r($data);
		// exit;

		$this->load->view('admin/establishments', $data);
	}

	public function users() {
		$this->load->model('users_model');
		$data['title'] = "Users";
		$data['small_title'] = "Manage list";
		$data['users'] = $this->users_model->get_users();
		$this->load->view('admin/users', $data);
	}

	public function insert_user(){
		$this->load->model('users_model');
		$this->load->helper(array('form', 'url', 'security'));
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('lname', 'Last Name', 'required|xss_clean');
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]|xss_clean');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[users.email]|xss_clean');
		$this->form_validation->set_rules('cel-num', 'Cellphone Number', 'required|is_unique[users.cel-num]|xss_clean');
		$this->form_validation->set_rules('birthdate', 'Birthdate', 'required|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'required|xss_clean');
		if ($this->form_validation->run() == FALSE) {
			echo validation_errors();
		} else {
			$data = $this->input->post();
			$this->users_model->add_user($data);
			echo 'Data Inserted Successfully';
		}
	} 

	public function delete_est() {
		$id = $this->uri->segment(3);
		$data['details'] = $this->admin_model->delete_establishments_where($id);
		
		$message = "success";
		$this->session->set_flashdata('message', $message);

		redirect(base_url().'admin/establishments');
	}

	public function view_est() {
		$id = $this->uri->segment(3);
		
		$data['title'] = "Establishments";
		$data['small_title'] = "Establishment Information";
		$data['details'] = $this->admin_model->get_establishments_where($id);

		$this->load->view('admin/establishments_view_details', $data);
	}

	public function update_est() {
		$id = $this->uri->segment(3);

		$data['title'] = "Establishments";
		$data['small_title'] = "Establishment Information";
		$data['details'] = $this->admin_model->get_establishments_where($id);
		$data['cities'] = $this->admin_model->get_cities();
		$data['provinces'] = $this->admin_model->get_provinces();
		$data['regions'] = $this->admin_model->get_regions();
		$data['zipcodes'] = $this->admin_model->get_zipcodes();
		$data['message'] = $this->session->flashdata('message');

		$data['link'] = "admin/add_establishment";

		$this->load->view('admin/establishments_update_details', $data);
	}

	public function add_est() {
		$id = $this->uri->segment(3);
		$post[0] = array(
			'id' => NULL,
			'location_id' => NULL,
			'sites_option_id' => NULL,
			'featured' => "no",
			'name' => NULL,
			'short_summary' => NULL,
			'description' => NULL,
			'image' => NULL,
			'cover_image' => NULL,
			'owner' => NULL,
			'friendly_url' => NULL,
			'price' => NULL,
			'booking_details' => NULL,
			'cards_accepted' => NULL,
			'wifi_ready' => NULL,
			'working_hours' => NULL,
			'email' => NULL,
			'telephone_number' => NULL,
			'cellphone_number' => NULL,
			'website' => NULL,
			'map' => NULL,
			'common_term' => NULL,
			'street' => NULL,
			'city_id' => NULL,
			'region_id' => NULL,
			'location_id' => NULL,
			'province_id' => NULL,
			'zipcode_id' => NULL,
		);

		$data['title'] = "Establishments";
		$data['small_title'] = "Establishment Information";
		$data['details'] = $post;
		$data['cities'] = $this->admin_model->get_cities();
		$data['provinces'] = $this->admin_model->get_provinces();
		$data['regions'] = $this->admin_model->get_regions();
		$data['zipcodes'] = $this->admin_model->get_zipcodes();
		$data['message'] = $this->session->flashdata('message');

		$data['link'] = "admin/add_establishment";

		$this->load->view('admin/establishments_update_details', $data);
	}

	public function add_establishment() {

		$post = $this->input->post(NULL, TRUE);
		
		$config['upload_path'] = './assets/images/img/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '10000KB';
		$config['max_width'] = '4096';
		$config['max_height'] = '4096';
		$this->load->library('upload', $config);

		$this->upload->initialize($config);

        if($this->upload->do_upload('coverphoto'))
		{
		    echo "hey na upload!";
		}
		else
		{
		   echo $this->upload->display_errors();
		}

		exit;
		echo "<pre>";
		print_r($post);
		exit;
	}

	public function update_establishment() {
		$post = $this->input->post(NULL, TRUE);


		$newloc = array(
			'street' => $post['street'],
			'common_term' => $post['common_term'],
			'city_id' => $post['city_id'],
			'province_id' => $post['province_id'],
			'region_id' => $post['region_id'],
			'zipcode_id' => $post['zipcode_id'],
		);

		$this->admin_model->update_locations($post['location_id'], $newloc);

		$newdata = array(
			'name' => $post['name'],
			'owner' => $post['owner'],
			'email' => $post['email'],
			'short_summary' => $post['short_summary'],
			'description' => $post['description'],
			'booking_details' => $post['booking_details'],
			'wifi_ready' => $post['wifi_ready'],
			'cards_accepted' => $post['cards_accepted'],
			'price' => $post['price'],
			'working_hours' => $post['working_hours'],
			'telephone_number' => $post['telephone_number'],
			'cellphone_number' => $post['cellphone_number'],
		);

		$message = "success";
		$this->admin_model->update_establishments($post['id'], $newdata);
		$this->session->set_flashdata('message', $message);

		redirect(base_url().'admin/update_est/'.$post['id']);
	}
}
