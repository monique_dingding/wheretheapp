<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public $username = "";
	public $data = array();
	function __construct() {
        parent::__construct();
        $this->username = "";
        $this->load->database();

       	if($this->session->userdata('logged_in'))
		   {
		      $session_data = $this->session->userdata('logged_in');
		      $data['username'] = $session_data['username'];
		      $this->username = $session_data['username'];
		      if($session_data['role_id']!=2)
		        redirect('login', 'refresh');
		   }
		   else
		   {
		     //If no session, redirect to login page
		     redirect('login', 'refresh');
		   }
    }


	public function index()
	{
	  $data['username'] = $this->username;
	  $this->load->view('admin/dashboard', $data);
	 }

	public function logout()
	 {
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('admin', 'refresh');
	 }

	public function establishments() {
		$this->load->model('admin_model');

		$data['title'] = "Establishments";
		$data['small_title'] = "Manage list";
		$data['establishments'] = $this->admin_model->get_establishments();

		$this->load->view('admin/establishments', $data);
	}

	public function users() {
		$this->load->model('users_model');
		$data['title'] = "Users";
		$data['small_title'] = "Manage list";
		$data['username'] = $this->username;
		$data['users'] = $this->users_model->get_users();
		$this->load->view('admin/users', $data);
	}

	public function user($id=NULL){
		$this->load->model('users_model');
		$this->load->helper(array('form', 'url', 'security'));
		$this->load->library('form_validation');
		$data['username'] = $this->username;
		$data['title'] = "User's Information";
		$data['small_title'] = "Update";
		$data['user'] = $this->users_model->get_user($id);
		$this->load->view('admin/user', $data);
	}

	public function delete_user($id=NULL)
	{
	   $this->load->model('users_model');
	   $this->users_model->delete_row($id, 'user_id','users');
	   redirect($_SERVER['HTTP_REFERER']);  
	}


	public function insert_user($id=NULL) {
		$this->load->model('users_model');
		$this->load->helper(array('form', 'url', 'security'));
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('lname', 'Last Name', 'required|xss_clean');

		if($id!=NULL) {
			$this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
			$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|xss_clean');
			$this->form_validation->set_rules('cel-num', 'Cellphone Number', 'required|xss_clean');
		}else {
			$this->form_validation->set_rules('pwd', 'Password', 'required|xss_clean|md5');
			$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]|xss_clean');
			$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[users.email]|xss_clean');
			$this->form_validation->set_rules('cel-num', 'Cellphone Number', 'required|is_unique[users.cel-num]|xss_clean');
		}

		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]|xss_clean');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[users.email]|xss_clean');
		$this->form_validation->set_rules('cel-num', 'Cellphone Number', 'required|is_unique[users.cel-num]|xss_clean');
		$this->form_validation->set_rules('birthdate', 'Birthdate', 'required|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE) {
			echo validation_errors();
		} else {
			$data = $this->input->post();
			if($id!=NULL){
				$this->users_model->update_user($id,$data);
				echo 'Data Updated Successfully';
			}else{
				$this->users_model->add_user($data);
				echo 'Data Inserted Successfully';
			}
		}
	}

}
