<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	function __construct() {
        parent::__construct();

        $this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
    }

    function process_output($output = NULL) {

    	$data['is_crud_table'] = TRUE;
    	$data['output'] = $output;

    	// echo "<pre>";
    	// print_r($output);
    	// exit;

    	$this->load->view('admin/crud_table_template', $data);
    
    }

    public function index() {

		$this->process_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

    function establishments() {

		try {
			$crud = new grocery_CRUD();
			$crud->set_primary_key('id');
			$crud->set_theme('flexigrid');
			$crud->set_table('sites');
			$crud->set_subject('Establishments');
			$crud->set_relation_n_n('option', 'sites_options', 'options', 'site_id', 'option_id', "option_name");
			$crud->add_fields('featured','name','owner','short_summary', 'description', 'image', 'cover_image', 'friendly_url', 'price', 'booking_details', 'cards_accepted', 'wifi_ready', 'working_hours', 'email', 'telephone_number', 'cellphone_number', 'website', 'map', 'option');
			$crud->edit_fields('featured','name','owner','short_summary', 'description', 'image', 'cover_image', 'friendly_url', 'price', 'booking_details', 'cards_accepted', 'wifi_ready', 'working_hours', 'email', 'telephone_number', 'cellphone_number', 'website', 'map', 'option');

			$crud->set_field_upload('image', 'assets/images/img', 'png|jpg|jpeg');
			$crud->set_field_upload('cover_image', 'assets/images/img', 'png|jpg|jpeg');
			$crud->required_fields('name', 'short_summary', 'description', 'image', 'cover_image');
			$crud->columns('id', 'name', 'featured', 'owner', 'cellphone_number', 'email', 'address');

			$crud->unset_texteditor('map');
			$crud->unset_jquery();
			$crud->unset_jquery_ui();
			$output = $crud->render();

			$this->process_output($output);

		} catch(Exception $e){ 
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function articles() {

		try {
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_primary_key('id');
			$crud->set_table('articles');
			$crud->set_subject('Articles');
			$crud->add_fields('author', 'subject', 'body', 'tags', 'main_image', 'secondary_image');
			$crud->edit_fields('author', 'subject', 'body', 'tags', 'main_image', 'secondary_image');

			$crud->set_field_upload('main_image', 'assets/images/img', 'png|jpg|jpeg');
			$crud->set_field_upload('secondary_image', 'assets/images/img', 'png|jpg|jpeg');
			// $crud->unset_texteditor('map');
			$crud->unset_jquery();
			$crud->unset_jquery_ui();
			$output = $crud->render();

			$this->process_output($output);

		} catch(Exception $e){ 
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}
}
