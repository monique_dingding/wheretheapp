<?php

class Admin_Model extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
    }

    function get_establishments() {
    	$this->db->select('sites.id as id, options.option, services.service_type, name, featured, owner, cellphone_number, telephone_number, email, locations.street, cities.city_name, provinces.province_name, regions.region_name, zip_codes.code');
        $this->db->from('sites');
        $this->db->join('sites_options', 'sites.id = sites_options.site_id');
        $this->db->join('options', 'options.id = sites_options.option_id');
        $this->db->join('sites_services', 'sites.id = sites_services.site_id');
        $this->db->join('services', 'services.id = sites_services.service_id');
        $this->db->join('locations', 'sites.location_id = locations.id');
        $this->db->join('cities', 'locations.city_id = cities.id');
        $this->db->join('provinces', 'locations.province_id = provinces.id');
        $this->db->join('regions', 'locations.region_id = regions.id');
        $this->db->join('zip_codes', 'locations.zipcode_id = zip_codes.id');
        $this->db->group_by("sites.id");
        $query = $this->db->get();

        return $query->result_array();
    }

    function get_establishments_where($id = NULL) {

        // $this->db->select('*');
        // $this->db->from('sites');
        // $this->db->where('id', $id);
        // $query = $this->db->get();


        $this->db->select('sites.id as id, sites.*, options.option, services.service_type, locations.*, cities.city_name, provinces.province_name, regions.region_name, zip_codes.code');
        $this->db->from('sites');
        $this->db->where('sites.id', $id);
        $this->db->join('sites_options', 'sites.id = sites_options.site_id');
        $this->db->join('options', 'options.id = sites_options.option_id');
        $this->db->join('sites_services', 'sites.id = sites_services.site_id');
        $this->db->join('services', 'services.id = sites_services.service_id');
        $this->db->join('locations', 'sites.location_id = locations.id');
        $this->db->join('cities', 'locations.city_id = cities.id');
        $this->db->join('provinces', 'locations.province_id = provinces.id');
        $this->db->join('regions', 'locations.region_id = regions.id');
        $this->db->join('zip_codes', 'locations.zipcode_id = zip_codes.id');
        $this->db->group_by("sites.id");
        $this->db->limit(1);
        $query = $this->db->get();
        
        return $query->result_array();


    }

    function update_establishments($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('sites', $data);
    }

    function delete_establishments_where($id) {
        $this->db->delete('sites', $id);
    }

    function get_cities() {
        $this->db->select('*');
        $this->db->from('cities');
        $query = $this->db->get();
        
        return $query->result_array();
    }

    function get_provinces() {
        $this->db->select('*');
        $this->db->from('provinces');
        $query = $this->db->get();
        
        return $query->result_array();
    }

    function get_regions() {
        $this->db->select('*');
        $this->db->from('regions');
        $query = $this->db->get();
        
        return $query->result_array();
    }

    function get_zipcodes() {
        $this->db->select('*');
        $this->db->from('zip_codes');
        $query = $this->db->get();
        
        return $query->result_array();
    }

    function update_locations($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('locations', $data);
    }
}
