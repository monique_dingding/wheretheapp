<?php

class Users_Model extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
    }

    function login($username, $password){
        $this->db->select('user_id, role_id, username, pwd');
        $this->db->from('users');
        $this->db->where('username', $username);
        $this->db->where('pwd', MD5($password));
        $this->db->limit(1);
     
        $query = $this -> db -> get();
     
        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    function get_establishments() {
    	$this->db->select('sites.id as id, options.option, services.service_type, name, owner, cellphone_number, telephone_number, email, locations.street, cities.city_name, provinces.province_name, regions.region_name, zip_codes.code');
        $this->db->from('sites');
        $this->db->join('sites_options', 'sites.id = sites_options.site_id');
        $this->db->join('options', 'options.id = sites_options.option_id');
        $this->db->join('sites_services', 'sites.id = sites_services.site_id');
        $this->db->join('services', 'services.id = sites_services.service_id');
        $this->db->join('locations', 'sites.location_id = locations.id');
        $this->db->join('cities', 'locations.city_id = cities.id');
        $this->db->join('provinces', 'locations.province_id = provinces.id');
        $this->db->join('regions', 'locations.region_id = regions.id');
        $this->db->join('zip_codes', 'locations.zipcode_id = zip_codes.id');
        $this->db->group_by("sites.id");
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_user($id=NULL,$data=NULL) 
    {
        $this->db->where('user_id', $id);
        $this->db->update('users', $data);              
    }

    function add_user($data){
        $this->db->insert('users', $data);
    }

    function delete_row($id,$column_name,$table_name)
    {
       $this->db->where($column_name, $id);
       $this->db->delete($table_name); 
    }

    function get_users() {
        $this->db->select('*');
        $this->db->from('users');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_user($id){
        $query = $this->db->get_where('users', array('user_id' => $id));
        return $query->result_array();
    }

    function get_role($username){
        $query = $this->db->get_where('users', array('username' => $username));
        return $query->row('role_id');
    }
}
