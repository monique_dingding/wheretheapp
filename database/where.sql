-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 27, 2015 at 07:01 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `where`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(100) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `body` text NOT NULL,
  `main_image` text NOT NULL,
  `secondary_image` text NOT NULL,
  `tags` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `author`, `subject`, `body`, `main_image`, `secondary_image`, `tags`) VALUES
(1, 'Monique Dingding', 'T’nalak Home Styles the Holiday Season ', '<p>\r\n	<strong>SUPPORT local.</strong> That&rsquo;s what us Pinoys are doing now. It may have been a long journey for the Filipino craftsmen to catch the attention of our nation, and they did, but maybe not after it received international recognition.<br />\r\n	<br />\r\n	Tadeco Livelihood has the same story. The abaca trade has been in this island for decades. With the product design getting an upgrade in the 90&rsquo;s, the abaca woven into T&rsquo;nalak fabric has excited the eyes and won the hearts of many, both foreign and local. No sooner, its products have become coveted.<br />\r\n	<br />\r\n	The Christmas collection is no different. At the TADECO Livelihood&rsquo;s concept store, T&rsquo;nalak Home, the Yuletide home decors have become must-haves. The parol, Christmas trees and other Holiday trimmings are now styled along with the traditional Western-inspired decorative pieces. The look achieved is striking as it is unique.<br />\r\n	<br />\r\n	On a larger scale, Abreeza Mall and Seda Abreeza Hotel, two properties of the Ayala&rsquo;s (they have been strong supporters of locally made products long before it became in vogue), commissioned T&rsquo;nalak Home to create its looks for the Holiday Season.<br />\r\n	<br />\r\n	For Abreeza Mall, T&rsquo;nalak Home took the old world sidewalk scene with lit lampposts illuminating cobblestone paths as its inspiration. Picking out the lamp to signify the &ldquo;Light of Christmas&rdquo; as the uniting figure, the result was a look and feel of elegance and festivity that merged Continental with Filipino on the most joyous time of the year.<br />\r\n	<br />\r\n	Custom-made &ldquo;gas lamps&rdquo; were fabricated according to design specifications. Bago-bago Vines were woven into form, colored in bright red and light up the Christmas installations along the mall&rsquo;s front gardens, doorways and interior hallways.<br />\r\n	<br />\r\n	But it&rsquo;s at the activity center that best showcases the theme. Large-scale lamps were suspended from the ceiling surrounding the giant Christmas tree, which is embellished with ribbons of handmade paper, balls made from abaca fiber in the season&rsquo;s hues and abaca twine stars in gold.<br />\r\n	<br />\r\n	It&rsquo;s the second year for T&rsquo;nalak Home to adorn Seda Abreeza Hotel with its distinctively Davao look. The Yuletide-themed decorative pieces made a perfect fit with the modern and minimalist makeup of the hotel, showing how easily stylishly designed products can fuse with contemporary interior design.<br />\r\n	<br />\r\n	&ldquo;Angels&rdquo; is the Seda hotel&rsquo;s chosen theme to represent &ldquo;beauty, goodness, protection, faith, hope and love.&rdquo; The heavenly deity is seen in its many forms&mdash;all handcrafted from natural materials&mdash;all over the establishment. It adorns tabletops, suspended from ceilings and trims the very impressive Christmas Trees, which were meticulously formed from handcrafted papers folded into cones. Pieced together, the Christmas trees mimic the feathers of angel wings.<br />\r\n	<br />\r\n	Truly, this holiday season, Dabawenyo ingenuity in design takes center stage at Abreeza Mall &amp; Seda Abreeza Hotel.<br />\r\n	<br />\r\n	T&rsquo;nalak Home is at the second level of Abreeza Mall.</p>\r\n', '3d16e-tnalak-homes-creations_0.jpg', '864c9-z1.jpg', 'local, davao, abreeza, seda');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=119 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name`) VALUES
(1, 'Alaminos City'),
(2, 'Angeles City'),
(3, 'Antipolo City'),
(4, 'Bacolod City'),
(5, 'Bago City'),
(6, 'Baguio City'),
(7, 'Bais City'),
(8, 'Balanga City'),
(9, 'Batangas City'),
(10, 'Bayawan City'),
(11, 'Bisilig City'),
(12, 'Butuan City'),
(13, 'Cabanatuan City'),
(14, 'Cadiz City'),
(15, 'Cagayan de Oro City'),
(16, 'Calamba City'),
(17, 'Calapan City'),
(18, 'Calbayog City'),
(19, 'Caloocan City'),
(20, 'Candon City'),
(21, 'Canlaon City'),
(22, 'Cauayan City'),
(23, 'Cavite City'),
(24, 'Cebu City'),
(25, 'Cotabato City'),
(26, 'Dagupan City'),
(27, 'Danao City'),
(28, 'Dapitan City'),
(29, 'Davao City'),
(30, 'Digos City'),
(31, 'Dipolog City'),
(32, 'Dumaguete City'),
(33, 'Escalante City'),
(34, 'Gapan City'),
(35, 'General Santos City'),
(36, 'Gingoog City'),
(37, 'Himamaylan City'),
(38, 'Iligan City'),
(39, 'Iloilo City'),
(40, 'Iriga City'),
(41, 'Isabela City'),
(42, 'Island Garden City of Samal'),
(43, 'Kabankalan City'),
(44, 'Kidapawan City'),
(45, 'Koronadal City'),
(46, 'La Carlota City'),
(47, 'Laoag City'),
(48, 'Lapu-Lapu City'),
(49, 'Las Pinas City'),
(50, 'Legazpi City'),
(51, 'Ligao City'),
(52, 'Lipa City'),
(53, 'Lucena City'),
(54, 'Maasin City'),
(55, 'Makati City'),
(56, 'Malabon City'),
(57, 'Malaybalay City'),
(58, 'Malolos City'),
(59, 'Malolos City'),
(60, 'Mandaluyong City'),
(61, 'Mandaue City'),
(62, 'Manila'),
(63, 'Maragondon'),
(64, 'Marawi City'),
(65, 'Masbate City'),
(66, 'Muntinlupa City'),
(67, 'Naga City'),
(68, 'Olongapo City'),
(69, 'Ormoc City'),
(70, 'Oroquieta City'),
(71, 'Ozamis City'),
(72, 'Pagadian City'),
(73, 'Palayan City'),
(75, 'Paranaque City'),
(76, 'Pasay City'),
(77, 'Pasig City'),
(78, 'Passi City'),
(79, 'Puerto Princesa City'),
(80, 'Quezon City'),
(81, 'Roxas City'),
(82, 'Sagay City'),
(83, 'San Carlos City, Neg'),
(84, 'San Carlos City, Pan'),
(85, 'San Fernando City, L'),
(86, 'San Fernando City, P'),
(87, 'San Jose City'),
(88, 'San Jose del Monte C'),
(89, 'San Pablo City'),
(90, 'Santa Rosa City'),
(91, 'Santiago City'),
(92, 'Mun City'),
(93, 'Silay City'),
(94, 'Sipalay City'),
(95, 'Sorsogon City'),
(96, 'Surigao City'),
(97, 'Tabaco City'),
(98, 'Tacloban City'),
(99, 'Tacurong City'),
(100, 'Tagaytay City'),
(101, 'Tagbilaran City'),
(102, 'Tagum City'),
(103, 'Talisay City, Cebu'),
(104, 'Talisay City, Negros'),
(105, 'Tanauan City'),
(106, 'Tangub City'),
(107, 'Tanjay City'),
(108, 'Tarlac City'),
(109, 'Taguig City'),
(110, 'Toledo City'),
(111, 'Trece Martires City'),
(112, 'Tuguegarao City'),
(113, 'Urdaneta City'),
(114, 'Valencia City'),
(115, 'Valenzuela City'),
(116, 'Victorias City'),
(117, 'Vigan City'),
(118, 'Zamboanga City');

-- --------------------------------------------------------

--
-- Table structure for table `establishments`
--

CREATE TABLE IF NOT EXISTS `establishments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `common_term` varchar(30) NOT NULL,
  `street` text NOT NULL,
  `city_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `zipcode_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `common_term`, `street`, `city_id`, `province_id`, `region_id`, `zipcode_id`) VALUES
(5, 'Davao City', 'Palma Gil St, Poblacion District', 29, 8, 13, 1),
(6, 'Samal', 'Kaputian', 42, 5, 12, 2),
(7, 'Davao City', 'Abreeza Ayala Business Park, J.P. Laurel Avenue', 29, 4, 13, 2);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `option_name`) VALUES
(1, 'eat'),
(2, 'stay'),
(3, 'drink'),
(4, 'unwind'),
(5, 'party'),
(6, 'buy souvenirs'),
(7, 'visit');

-- --------------------------------------------------------

--
-- Table structure for table `photos_slider`
--

CREATE TABLE IF NOT EXISTS `photos_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `thumbnail` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `photos_slider`
--

INSERT INTO `photos_slider` (`id`, `site_id`, `image`, `thumbnail`) VALUES
(1, 5, 'royalmandaya-1.jpg', 'royalmandaya-1-thumb.jpg'),
(2, 5, 'royalmandaya-2.jpg', 'royalmandaya-2-thumb.jpg'),
(3, 5, 'royalmandaya-3.jpg', 'royalmandaya-3-thumb.jpg'),
(4, 6, 'pearlfarm-1.jpg', 'pearlfarm-1-thumb.jpg'),
(5, 6, 'pearlfarm-2.jpg', 'pearlfarm-2-thumb.jpg'),
(6, 6, 'pearlfarm-3.jpg', 'pearlfarm-3-thumb.jpg'),
(8, 7, 'sedadavao-1.jpg', 'sedadavao-1-thumb.jpg'),
(9, 7, 'sedadavao-2.jpg', 'sedadavao-2-thumb.jpg'),
(10, 7, 'sedadavao-3.jpg', 'sedadavao-3-thumb.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE IF NOT EXISTS `provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `province_name`) VALUES
(1, 'Palawan'),
(2, 'Aklan'),
(3, 'Bohol'),
(4, 'Davao del Sur'),
(5, 'Davao del Norte'),
(6, 'Davao Oriental'),
(7, 'Albay'),
(8, 'None');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_name` varchar(25) NOT NULL,
  `region_number` varchar(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `region_name`, `region_number`) VALUES
(1, 'Autonomous Region in Musl', 'ARMM'),
(2, ' Bicol Region', 'V'),
(3, 'Cordillera Administrative', 'CAR'),
(4, 'Cagayan Valley', 'II'),
(5, 'SOCCSKSARGEN', 'XII'),
(6, 'Central Luzon', 'III'),
(7, 'Caraga', 'XIII'),
(8, 'Central Visayas', 'VII'),
(9, 'Eastern Visayas', 'VIII'),
(10, 'Ilocos Region', 'I'),
(11, 'National Capital Region', 'NCR'),
(12, 'Northern Mindanao', 'X'),
(13, 'Davao Region', 'XI'),
(14, 'Zamboanga Peninsula', 'IX'),
(15, 'Western Visayas', 'VI'),
(16, 'MIMAROPA', 'IV-B'),
(17, 'CALABARZON', 'IV-A');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `description`) VALUES
(1, 'client'),
(2, 'admin'),
(3, 'super admin');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_type`) VALUES
(1, 'Resort'),
(2, 'Restaurant'),
(3, 'Coffee Shop'),
(4, 'Hotel');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE IF NOT EXISTS `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `sites_option_id` int(11) NOT NULL,
  `featured` enum('yes','no') NOT NULL DEFAULT 'no',
  `name` varchar(30) NOT NULL,
  `short_summary` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `address` text NOT NULL,
  `image` text NOT NULL,
  `cover_image` text NOT NULL,
  `owner` varchar(50) NOT NULL,
  `friendly_url` varchar(40) NOT NULL,
  `price` varchar(30) NOT NULL,
  `booking_details` text NOT NULL,
  `cards_accepted` enum('yes','no') NOT NULL DEFAULT 'no',
  `wifi_ready` enum('yes','no') NOT NULL DEFAULT 'no',
  `working_hours` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telephone_number` varchar(20) NOT NULL,
  `cellphone_number` varchar(30) NOT NULL,
  `website` varchar(100) NOT NULL,
  `map` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `location_id`, `sites_option_id`, `featured`, `name`, `short_summary`, `description`, `address`, `image`, `cover_image`, `owner`, `friendly_url`, `price`, `booking_details`, `cards_accepted`, `wifi_ready`, `working_hours`, `email`, `telephone_number`, `cellphone_number`, `website`, `map`) VALUES
(5, 5, 1, 'no', 'Royal Mandaya Hotel', 'At The Royal Mandaya Hotel, it’s truly a royal experience.', '<p>\r\n	Be awestruck by the cavalcade of views as you retreat in the cozy, luxurious villas and cottages of Pearl Farm Beach Resort. Offering reasonable rates, our accommodations cater to honeymooning couples, adventure-seeking families, leisure travelers, and business executives. All suites and villas have a balcony that serves as a perfect spot from which to catch romantic views of the sunrise, sunset, and the calm sea. Each lodging in our beach resort in Samal Island has many handy amenities to give you the best comfort.</p>\r\n', 'Palma Gil St, Poblacion District, Davao City, Davao del Sur, Philippines 8000', 'royalmandaya.jpg', 'royalmandaya-cover.jpg', 'Jackelyn Jose', 'royal-mandaya-hotel', 'P2100 to P12000', '<p>\r\n	One older child or adult is charged PHP 819.67 per person per night in an extra bed. The maximum number of extra beds in a room is 1. There is no capacity for cribs in the room. Any type of extra bed or crib is upon request and needs to be confirmed by management. Additional fees are not calculated automatically in the total cost and will have to be paid for separately during your stay.</p>\r\n', 'no', 'no', 'Monday - Friday, 7AM to 10PM', 'admin@royalmandayahotel.com', '+63 82 285 0601', '+639 448 7641', 'http://www.royalmandayahotel.com', ''),
(6, 6, 2, 'yes', 'Pearl Farm Beach Resort', 'Samal Island in the Philippines has long been a jealously-guarded secret for citizens of nearby Davao City.', '<p>\r\n	The 14-hectare Pearl Farm Resort was an actual working pearl farm years and years ago, until the owners saw the location&#39;s potential as a beach retreat and acted on it.The landscape is set at a remove from the Davao City - ferry rides from the city to the resort take a good 40 minutes to make the crossing - but the resort makes the most out of the area&#39;s relative seclusion and the unique local geography.</p>\r\n', 'ONB Babak Building, Babak - Samal - Kaputian Rd, Babak, Island Garden City of Samal, 8119 Davao del Norte, Philippines', 'pearlfarm.jpg', 'pearlfarm-cover.jpg', 'Vin Smeagle', 'pearl-farm-beach-resort', 'P399 to P4000', '', 'no', 'no', '', '', '+02 228 6236', '+639 089 8970', '', ''),
(7, 7, 3, 'yes', 'Seda Abreeza Hotel Davao', 'Seda Abreeza’s interiors are sleek, clean and elegant.', '<p>\r\n	Seda Abreeza offers 186 guestrooms for discerning travelers searching for the best hotels in Davao City, Southern Mindanao Island&rsquo;s hub for commerce, tourism and industry. A key element of the upscale Abreeza mall, the hotel ushers guests to a world of unparalleled comfort and convenience, and reflects the city&rsquo;s progress with cutting-edge features that include an e-lounge with a bank of iMacs at the lobby and complimentary Wi-Fi throughout the premises.</p>\r\n', 'Abreeza Ayala Business Park, J.P. Laurel Ave, Poblacion District, Davao City, Davao del Sur, Philippines', 'sedadavao.jpg', 'sedadavao-cover.jpg', 'Accendo Commercial Corporation', 'seda-abreeza-davao', 'PHP 3,650.00 to PHP 7,900.00', '<p>\r\n	All rooms have:<br />\r\n	40-inch LED HDTV with cable channels<br />\r\n	Media panel with HDMI, USB and A/V connectivity<br />\r\n	Speaker Phone with iPod dock and FM Stereo<br />\r\n	Executive Writing Desk<br />\r\n	Complimentary Wi-Fi<br />\r\n	Voice Mail<br />\r\n	Alarm Clock<br />\r\n	Coffee &amp; tea making facilities<br />\r\n	Minibar<br />\r\n	Hair Dryer</p>\r\n', 'no', 'no', '', 'info@sedabreeza.com', '+082 241 5706', '+639 147 8713', 'www.sedahotels.com/abreeza/', '');

-- --------------------------------------------------------

--
-- Table structure for table `sites_options`
--

CREATE TABLE IF NOT EXISTS `sites_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `sites_options`
--

INSERT INTO `sites_options` (`id`, `site_id`, `option_id`) VALUES
(8, 1, 1),
(9, 2, 3),
(10, 5, 3),
(11, 5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sites_services`
--

CREATE TABLE IF NOT EXISTS `sites_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sites_services`
--

INSERT INTO `sites_services` (`id`, `site_id`, `service_id`) VALUES
(1, 5, 1),
(2, 5, 2),
(3, 6, 1),
(4, 6, 3),
(5, 7, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pwd` varchar(250) NOT NULL,
  `cel-num` varchar(12) NOT NULL,
  `tel-num` varchar(12) NOT NULL,
  `birthdate` date NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `role_id`, `fname`, `lname`, `username`, `email`, `pwd`, `cel-num`, `tel-num`, `birthdate`, `status`) VALUES
(3, 0, 'chit', 'chit', 'chit', 'chit@dsd', 'chit', '21121', '2312', '2015-11-13', 1),
(4, 0, 'kjlkl', 'klklk', 'lklklkl', 'lkl@klkl', 'klklk', 'klkl', 'klkl', '2015-11-09', 1),
(5, 0, 'ddfg', 'dfgdg', 'dgdfg', 'sdfsdf@dsfsf.com', 'fg', 'dsfsdf', '', '2015-12-01', 1),
(6, 0, 'dfsf', 'sfsfd', 'adasd', 'asdasd@sdsd.com', 'jkjk', 'sdasd', '', '2015-11-27', 1),
(8, 0, 'jkjkj', 'jkjkj', 'jkjkjkj', 'jkjkj@sdfsdf.com', 'jkjk', 'jkjk', 'jkjk', '2015-11-12', 1),
(9, 0, 'chit@dsd', 'chit@dsd', 'chit@dsd', 'chit@dsd.com', 'chit@dsd', 'chit@dsd', 'chit@gmail.c', '2015-11-27', 1),
(11, 2, 'chit1', 'chit1', 'chit1', 'chit1@yahoo.com', '11ef83b04575675dafa93f40e32caabe', 'chit1', 'chit1', '2015-12-11', 1),
(12, 1, 'Client', 'Client', 'Client', 'Client@yahoo.com', '577d7068826de925ea2aec01dbadf5e4', 'Client', 'Client', '2015-12-14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `zip_codes`
--

CREATE TABLE IF NOT EXISTS `zip_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `zip_codes`
--

INSERT INTO `zip_codes` (`id`, `code`) VALUES
(1, '8000'),
(2, '8119');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
